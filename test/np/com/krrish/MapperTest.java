package np.com.krrish;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class MapperTest {

    @Test
    void testEmptyFieldsMap() {
        Source source = new Source();
        Object destination = Mapper.map(source, Destination.class);
        assertEquals(Destination.class, destination.getClass());
    }

    @Test
    void testMapWithSameFields() {
        Source source = new Source();
        source.setFirst("first");
        source.setSecond("second");

        Destination destination = (Destination) Mapper.map(source, Destination.class);
        assertEquals("first", destination.getFirst());
    }
}
