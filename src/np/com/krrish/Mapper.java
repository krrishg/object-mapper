package np.com.krrish;

import java.lang.reflect.*;

public class Mapper {
    public static Object map(Object source, Class destination) {
        Object object = new Object();
        try {
            Class<?> aClass = Class.forName(destination.getName());
            object = aClass.getConstructor().newInstance();
            Field[] sourceFields = source.getClass().getDeclaredFields();
            for (Field sourceField : sourceFields) {
                if (!sourceField.isSynthetic()) {
                    sourceField.setAccessible(true);
                    Field destinationField = object.getClass().getDeclaredField(sourceField.getName());
                    destinationField.setAccessible(true);
                    destinationField.set(object, sourceField.get(source));
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException ignored) {
        }
        return object;
    }
}
