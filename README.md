# Where to get the library?

Download the jar file from build/libs and include it in your project

# How to use?

import np.com.krrish.Mapper;

DestinationObject destinationObject = (DestinationObject) Mapper.map(sourceObject, DestinationObject.class);
